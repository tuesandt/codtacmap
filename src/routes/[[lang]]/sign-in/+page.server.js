import { AuthApiError } from '@supabase/supabase-js';
import { fail, redirect } from '@sveltejs/kit';
import { zod } from 'sveltekit-superforms/adapters';
import { superValidate } from 'sveltekit-superforms/server';
import { z } from 'zod';

export async function load({ locals }) {
    let { session } = await locals.safeGetSession();
	console.log(session)
	if (session) redirect(303, '/');
}

export const actions = {
    default: async ({ request, url, locals }) => {
		const form = await superValidate(request, zod(signInSchema));
		
        if (!form.valid) {
            return fail(400, {
                form
            })
        }

        const { data, error } = await locals.supabase.auth.signInWithOtp({
			email: form.data.email,
			options: {
				emailRedirectTo: url.origin + '/auth/callback',
				shouldCreateUser: false
			}
		});

		if (error instanceof AuthApiError && error.status === 400) {
			return fail(400, {
				form,
				message: 'This user does not exist'
			});
		}

		if (error instanceof AuthApiError && error.status === 429) {
			return fail(429, {
				form,
				message: 'Email rate limit exceeded'
			});
		}


        if (!error) {
			return {
				form,
				email: form.data.email
			};

		} else {
			if (error instanceof AuthApiError && error.status === 400) {
				return fail(400, {
					form,
					message: error.message
				});
			}

			return fail(500, {
				form,
				message: 'Server error. Try again later.'
			});
		}
    }
}

const signInSchema = z.object({
	email: z.string().trim().min(1).email()
});