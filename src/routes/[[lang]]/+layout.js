import { error } from '@sveltejs/kit';

export async function load ({ params }) {
    let { lang = 'en' } = params;
    let supportedLang = ['ua'];

    if ( lang !== 'en' && !supportedLang.includes(lang) ) {
        error(404, {
            message: 'Something went wrong!'
        })
    }

    return {
        lang: lang == 'en' ? '' : lang
    }
}