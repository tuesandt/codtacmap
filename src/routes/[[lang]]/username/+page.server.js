import { fail, redirect } from '@sveltejs/kit';
import { superValidate } from 'sveltekit-superforms/server';
import { z } from 'zod';

let setUsernameSchema;

export const load = async ({ locals, cookies }) => {
    setUsernameSchema = z.object({
		username: z
			.string()
			.trim()
            .min(1, { message: 'Must have at least 1' })
			.max(15)
			.regex(/^[a-zA-Z0-9]+$/, { message: 'Only Latin characters are allowed' })
			.refine(username => !/^\d+$/.test(username), { message: 'Username cannot be only numbers' })
			.refine(username => username.toLowerCase() !== 'z', { message: 'Something wrong with your username' })
	})
	.refine(
		async ({ username }) => {
			const { error } = await locals.supabase
				.from('profiles')
				.select('id')
				.eq('username', username)
				.single();

			if (error) {
				return true;
			} else {
				return false;
			}
		},
		{
			message: 'This user already exists',
			path: ['username'] // path of error
		}
	);
}

export const actions = {
    default: async ({ request, locals }) => {
        const session = await locals.getSession();
        if (!session) return; 

        const formData = await request.formData();
        const form = await superValidate(formData, setUsernameSchema);

        if (!form.valid) {
            return fail(400, {
                form
            })
        }

        const { data, error } = await locals.supabase
            .from('profiles')
            .update({
                username: form.data.username
            })
            .eq('id', session.user.id)
            .select('*');   

        console.log(data, error)

        if (!error) {
            redirect(303, '/');
        } else {
            return fail(400, {
                form
            })
        }


    }
};