import { AuthApiError } from '@supabase/supabase-js';
import { fail, redirect } from '@sveltejs/kit';
import { zod } from 'sveltekit-superforms/adapters';
import { superValidate } from 'sveltekit-superforms/server';
import { z } from 'zod';

let signUpSchema;
const blockedDomains = ["mail.ru", 'yandex.ru', 'yandex.ua', 'bk.ru', 'inbox.ru', 'list.ru', 'rambler.ru'];

export async function load({ locals }) {
    let { session } = await locals.safeGetSession();
	if (session) redirect(303, '/');

    signUpSchema = z.object({
        email: z.string().trim().email()
    })
    .refine(async ({ email }) => {
        const domain = email.split('@')[1];
        return !blockedDomains.includes(domain);
    }, {
        message: 'This email domain does not exist',
        path: ['email'] // path of error
    })
    .refine(
        async ({ username }) => {
            const { data: userData, error: userError } = await locals.supabase
                .from('profiles')
                .select('*')
                .eq('username', username)
                .single();

            if (userData) {
                return false;
            } else {
                return true;
            }
            // return true;
        },
        {
            message: 'This user already exists',
            path: ['username'] // path of error
        }
    );
}

export const actions = {
    default: async ({ request, url, locals }) => {
        // const formData = await request.formData();
        const form = await superValidate(request, zod(signUpSchema));

        if (!form.valid) {
            return fail(400, {
                form
            })
        }

		const { data: userData, error: userError } = await locals.supabase.auth.signInWithOtp({
			email: form.data.email,
			options: {
				data: {
					username: form.data.username
				},
				// emailRedirectTo: `${import.meta.env.VITE_VERCEL_WEBSITE_URL}/auth/callback`
				emailRedirectTo: `${url.origin}/auth/callback`
			}
		});


        if (!userError) {
			// throw redirect(303, '/');
            return {
                form,
                message: `The account has been successfully created. To activate it, find our letter at the email address you provided (${form.data.email}) and follow the link.`,
            }
		} else {
			if (userError instanceof AuthApiError && userError.status === 400) {
				return fail(400, {
                    form,
					message: 'Invalid email or password'
				});
			}

			return fail(500, {
                form,
				message: 'Something Wrong! Please try again later'
			});
		}

    }
};