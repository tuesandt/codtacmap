import { fail } from '@sveltejs/kit';
import uniqid from 'uniqid';

import { superValidate, withFiles } from 'sveltekit-superforms';
import { zod } from 'sveltekit-superforms/adapters';
import { z } from 'zod';

export async function load({ locals, params, cookies }) {
	const { session } = await locals.safeGetSession();
	const profile = await locals.getProfile();
	const slug = params.map;

	const fetchMapData = async (slug) => {
		const { data: mapData, error: mapError } = await locals.supabase
			.from('maps')
			.select('*')
			.eq('url', slug)
			.single()

		// const { data: previewData, error: previewError } = await locals.supabase.storage
		// 	.from('maps')
		// 	.getPublicUrl(`${mapData.url}`);
		// mapData.previewUrl = previewData.publicUrl;

		// console.log(mapData.url)

		if (!mapData) return;

		if (mapData.meta_image) {
			const { data: metaImageData, error: metaImageError } = await locals.supabase.storage
			.from('maps')
			.getPublicUrl(`meta/${mapData.meta_image}`);

			mapData.ogImage = metaImageData.publicUrl;
		}

		const { data: textureData, error: textureError } = await locals.supabase.storage
			.from('maps')
			.getPublicUrl(`texture/${mapData.url}`);
		mapData.texture = textureData.publicUrl;

		if (mapError) return {};
		return mapData;
	};

	const fetchSidebarNavList = async (mapId) => {
		let queryInArray = ['guest'];
		if (session) queryInArray.push('user')
		if (profile?.role == 'admin') queryInArray.push('admin')

		const { data, error } = await locals.supabase.from('sidebar_menu')
			.select('*')
			.eq('map', mapId)
			.neq('category', 'settings')
			.in('access', queryInArray);

		if (error) return [];

		if (cookies.get('ctmSidebarNav')) {
			let cookie = JSON.parse(cookies.get('ctmSidebarNav'));
			if (cookie[params.map]) {
				data.map((item) => {
					item.visible = cookie[params.map][item.code_name] === 'on';
				});
			}
		}

		return data;
	}

	function getMapSettings () {
		let defaultSettings = {
			mapNoise: 1,
			mapGray: 0,
			mapGrid: 1,
			mapInvert: 0
		}

		if (cookies.get('ctmSidebarSettings')) {
			let cookie = JSON.parse(cookies.get('ctmSidebarSettings'));
			if (cookie[params.map]) {

				for (const item in defaultSettings) {
					defaultSettings[item] = cookie[params.map][item] === 'on';
				}

				// data.map((item) => {
				// 	item.visible = cookie[params.map][item.code_name] === 'on';
				// });
			}
		}

		return defaultSettings;
	}

	function fetchSidebarFilter () {
		let array = [];

		if (cookies.get('wtmSidebarFilter')) {
			let cookie = JSON.parse(cookies.get('wtmSidebarFilter'));
			if (cookie[params.map]) {
				Object.keys(cookie[params.map]).forEach(key => {
					array.push(key);
				});
				return array;
			}
		}	
	}

	const fetchMarkers = async (slug) => {
		let query = locals.supabase
			.from('markers')
			.select('id, name, description, type, location, group, icon, markers_extra(styles)')
			.eq('category', 'global')
			.eq('map', slug);

		let { data } = await query.then((r) => r);

		data = data || [];

		let groupedMarkers = data.reduce((array, item) => {
				const { group } = item;
				if (!array[group])
					array[group] = {
						list: [],
						visible: false
					};
				array[group].list.push(item);
				return array;
			},
			{
				grid: {
					visible: true
				}
			}
		);
			
		let cookieSidebarNav = cookies.get('ctmSidebarNav');
		if (cookieSidebarNav !== undefined) {
			let cookie = JSON.parse(cookieSidebarNav)[params.map];
			// console.log(cookie)
			if (cookie == undefined) return false;
			Object.keys(cookie).forEach((key) => {
				if (groupedMarkers[key] !== undefined) groupedMarkers[key].visible = true;
			});
			// Оскільки грід по дефолту true, то він буде завжди увімкнений якщо не зробити це
			if (cookie.grid == undefined) groupedMarkers['grid'].visible = false; // якщо є кукі, але немає гріда, значить він має бути вимкнений
		} else {
			sidebarNavList
				.filter((item) => item.visible == true)
				.forEach((item) => {
					if (groupedMarkers[item.code_name] !== undefined)
						groupedMarkers[item.code_name].visible = true;
				});
		}

		let cookieSidebarNavSettings = cookies.get('ctmSidebarSettings');
		if (cookieSidebarNavSettings !== undefined) {
			let cookie = JSON.parse(cookieSidebarNavSettings)[params.map];
			// console.log(cookie)
			if (cookie == undefined) return false;
			Object.keys(cookie).forEach((key) => {
				if (groupedMarkers[key] !== undefined) groupedMarkers[key].visible = true;
			});
			// Оскільки грід по дефолту true, то він буде завжди увімкнений якщо не зробити це
			if (cookie.grid == undefined) groupedMarkers['grid'].visible = false; // якщо є кукі, але немає гріда, значить він має бути вимкнений
		} else {
			sidebarNavList
				.filter((item) => item.visible == true)
				.forEach((item) => {
					if (groupedMarkers[item.code_name] !== undefined)
						groupedMarkers[item.code_name].visible = true;
				});
		}

		return groupedMarkers;
	};

	const fetchIcons = async () => {
		let { data: iconsData } = await locals.supabase.from('icons').select('*');

		for (const icon of iconsData) {
			let { data: iconData, error: iconError } = await locals.supabase.storage
				.from('icons')
				.getPublicUrl(icon.file_name);
			icon.path = iconData.publicUrl;
		}

		return iconsData;
	};

	const mapData = await fetchMapData(slug);
	const sidebarNavList = await fetchSidebarNavList(mapData.id);
	const sidebarNavSettings =  getMapSettings();

	return {
		mapData,
		sidebarNavList,
		sidebarNavSettings,
		markersData: await fetchMarkers(mapData.id),
		icons: await fetchIcons(),
		sidebarFilter: fetchSidebarFilter()
	};
}

export const actions = {
	sidebarFilter: async ({ request, cookies, params }) => {
		const formData = Object.fromEntries(await request.formData());
		delete formData['sidebarFilterInput'];
		let cookie = cookies.get('wtmSidebarFilter');
		cookie = cookie ? JSON.parse(cookie) : {};
		cookie[params.map] = formData;
		cookies.set('wtmSidebarFilter', JSON.stringify(cookie), {
			path: '/',
			maxAge: 60 * 60 * 24 * 365,
			httpOnly: false // <-- if you want to read it in the browser
		});
	},


	sidebarNavList: async ({ request, cookies, params }) => {
		const formData = Object.fromEntries(await request.formData());
		let cookie = cookies.get('ctmSidebarNav');

		cookie = cookie ? JSON.parse(cookie) : {};
		cookie[params.map] = formData;
		cookies.set('ctmSidebarNav', JSON.stringify(cookie), {
			path: '/',
			maxAge: 60 * 60 * 24 * 365,
			httpOnly: false // <-- if you want to read it in the browser
		});
	},

	sidebarNavSettings: async ({ request, cookies, params }) => {
		const formData = Object.fromEntries(await request.formData());
		let cookie = cookies.get('ctmSidebarSettings');

		cookie = cookie ? JSON.parse(cookie) : {};
		cookie[params.map] = formData;
		cookies.set('ctmSidebarSettings', JSON.stringify(cookie), {
			path: '/',
			maxAge: 60 * 60 * 24 * 365,
			httpOnly: false // <-- if you want to read it in the browser
		});
	},

	addMarker: async ({ request, locals, params }) => {
		const session = await locals.safeGetSession();
		const profile = await locals.getProfile();
		const accessRole = await locals.getAccessRole();

		if (!session || !accessRole.includes(profile.role)) return false;

        // const formData = await request.formData();
		// const file = formData.get('file');
		const form = await superValidate(request, zod(addMarkerSchema));

        if (!form.valid) {
            return fail(400, withFiles({
				form
			}))
        }

		const uniqID = uniqid();
		let imageData;
		let fileName;

		if (form.data.file) {
			const fileName = `preview-${uniqID}`
            const { data: _imageData, error: imageError } = await locals.supabase.storage.from('markers_images').upload(fileName, form.data.file)
			console.log(imageError)
            if (imageError) {
				return fail(400, withFiles({
					form
				}))
            }
            imageData = _imageData;
		}

		console.log(imageData)

		// if (file.size > 0) {
        //     if (file.size > 1048576) {
        //         form.errors.file = ['form.error.fileSize']
        //         return fail(400, {
        //             form
        //         });
        //     }
        //     const fileName = `preview-${uniqID}`
        //     const { data: _imageData, error: imageError } = await locals.supabase.storage.from('markers_images').upload(fileName, file)
        //     if (imageError) {
        //         return fail(400, {
        //             form
        //         });
        //     }
        //     imageData = _imageData;
        // }

		// return;

        const insertData = {
            name: form.data.name,
            description: form.data.description,
            location: {
                lat: form.data.lat,
                lng: form.data.lng
            },
            icon: form.data.icon,
            group: form.data.group,
            category: form.data.category,
            type: form.data.type,
            is_published: form.data.published,
            map: form.data.map,
            author: profile.id,
			// file: form.data.file
        }


        

		const { data, error } = await locals.supabase
		.from('markers')
		.insert(insertData)
		.select('*')
		.single()
		.then(async ( { data } ) => {
			let insertExtraData = { 
				marker_id: data.id,
				is_regular_spawn: form.data.regularSpawn
				// is_regular_spawn: form.data
			}
			if (imageData) insertExtraData.preview = imageData.path
		  	const { data: extraData } = await locals.supabase.from('markers_extra').insert(insertExtraData).select('*').single()
			  data.markers_extra = extraData
			return {
				data: data
			};
		});


		console.log('after', data);

        // const { data, error } = await locals.supabase
        //     .from('markers')
        //     .insert(insertData)
        //     .select('*')
        //     .single();

		

        if(error) return fail(400, withFiles({ form, error }))
    
        return withFiles({
            form,
            message: 'Bla bla. marker added',
            marker: data
        })
	},

	editMarker: async ({ request, locals, params }) => {
		const session = await locals.safeGetSession();
		const profile = await locals.getProfile();
		const accessRole = await locals.getAccessRole();
		if (!session || !accessRole.includes(profile.role)) return false;

		const form = await superValidate(request, zod(editMarkerSchema));

        if (!form.valid) {
            return fail(400, withFiles({
				form
			}))
        }

		const uniqID = uniqid();
		let imageData;
		let fileName;

		if (form.data.file) {
			const fileName = `preview-${uniqID}`
            const { data: _imageData, error: imageError } = await locals.supabase.storage.from('markers_images').upload(fileName, form.data.file)

            if (imageError) {
				return fail(400, withFiles({
					form
				}))
            }

			const { data: markerData, error: markerError } = await locals.supabase.from('markers_extra').select('preview').eq('marker_id', form.data.id).single();
		
			if (markerData.preview) {
				const { data: markerImageData, error: markerImageError } = await locals.supabase
					.storage
					.from('markers_images')
					.remove([markerData.preview])
			}

            imageData = _imageData;
		}


		const insertData = {
            name: form.data.name,
            description: form.data.description,
            icon: form.data.icon,
            group: form.data.group,
            category: form.data.category,
            type: form.data.type,
            is_published: form.data.published,
        }

		const { data, error } = await locals.supabase
			.from('markers')
			.update(insertData)
			.select('*')
			.eq('id', form.data.id)
			.single()
			.then(async ( { data } ) => {

				let insertExtraData = { 
					// marker_id: data.id,
					is_regular_spawn: form.data.regularSpawn,
					// is_regular_spawn: form.data
				}

				if (imageData) {
					insertExtraData.preview = imageData.path
				}

				const { data: extraData, error: extraError } = await locals.supabase
					.from('markers_extra')
					.update(insertExtraData)
					.select('*')
					.eq('marker_id', form.data.id)
					.single()

					console.log(extraData, extraError)

					data.markers_extra = extraData

					return {
						data: data
					}

				// let insertExtraData = { 
				// 	marker_id: data.id,
				// 	is_regular_spawn: form.data.regularSpawn,
				// 	// is_regular_spawn: form.data
				// }
				// if (imageData) insertExtraData.preview = imageData.path
				//   const { data: extraData } = await locals.supabase.from('markers_extra').insert(insertExtraData).select('*').single()
				//   data.markers_extra = extraData
				// return {
				// 	data: data
				// };
			});


		if(error) return fail(400, withFiles({ form, error }))

        return withFiles({
            form,
            message: 'Bla bla. marker added',
            marker: data
        })

	},

	removeMarker: async ({ request, locals, params }) => {
		const session = await locals.safeGetSession(), 
		profile = await locals.getProfile(),
		accessRole = await locals.getAccessRole();

		if (!session || !accessRole.includes(profile.role)) return fail(403, { message: 'Forbidden' });

		// const formData = await request.formData();
		const form = await superValidate(request, zod(removeMarkerSchema));

        if (!form.valid) {
            return fail(400, {
                form,
				message: 'Bla lba'
            })
        }

		const { data: markerData, error: markerError } = await locals.supabase
			.from('markers')
			.select('id, markers_extra( preview )')
			.eq('id', form.data.id)
			.single()
			.then(async( { data } ) => {
				if (data.markers_extra.preview) {
					// cloudinary.uploader.destroy(data.markers_extra.cloudinary_id, function(error,result) {
					// 	console.log(result, error);
					// });

					const { data: markerImageData, error: markerImageError } = await locals.supabase
						.storage
						.from('markers_images')
						.remove([data.markers_extra.preview])
				}

				return {
					data: data
				}
			});



		const { data, error } = await locals.supabase
			.from('markers')
			.delete()
			.eq('id', form.data.id)


		if(error) return fail(400, { form, error })

		return {
			form
		}
		
	},


	sidebarAddNewItem: async ({ request, locals, params }) => {
		const { session } = await locals.safeGetSession();
		if (!session) return;

		const form = await superValidate(request, zod(addSidebarItemSchema));
        if (!form.valid) {
            return fail(400, {
				form
			})
        }

		const insertData = {...form.data}

		const { data, error } = await locals.supabase
			.from('sidebar_menu')
			.insert(insertData)
			.select('*')
			.single()
	
		console.log(data, error);
	}
}

const addSidebarItemSchema = z.object({
	name: z.string().min(1).max(40),
	category: z.string(),
	visible: z.boolean(),
	map: z.number(),
	access: z.string(),
	code_name: z.string(),
});

const addMarkerSchema = z.object({
    lat: z.string(),
    lng: z.string(),
    name: z.string().min(1).max(40),
    published: z.boolean(),
	regularSpawn: z.boolean(),
    type: z.string().min(1).max(40),
    icon: z.string(),
    group: z.string(),
    category: z.string(),
    map: z.number(),
	
	// file: z.custom((f) => f instanceof File, 'Please upload a file.').refine((f) => f.size < 1048576, 'Max 100 kB upload size.').nullable().optional(),
    fileName: z.string().default("").optional(),
	file: z.custom((f) => f instanceof File, 'Please upload a file.').nullable().optional(),
	description: z.string().max(2000).default(""),
})

const editMarkerSchema = z.object({
	id: z.number(),
    lat: z.string(),
    lng: z.string(),
    name: z.string().min(1).max(40),
    published: z.boolean(),
	regularSpawn: z.boolean(),
    type: z.string().min(1).max(40),
    icon: z.string(),
    group: z.string(),
    category: z.string(),
    map: z.number(),
    
	// file: z.custom((f) => f instanceof File, 'Please upload a file.').refine((f) => f.size < 1048576, 'Max 100 kB upload size.').nullable().optional(),
    file: z.custom((f) => f instanceof File, 'Please upload a file.').nullable().optional(),
	description: z.string().max(2000).default(""),
})

const removeMarkerSchema = z.object({
	id: z.number(),
});