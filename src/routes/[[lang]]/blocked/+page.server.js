import { redirect } from '@sveltejs/kit';

export async function load({ locals }) {
    const session = await locals.getSession();
    const profile = await locals.getProfile();

    if (session) {
        if (!profile.is_blocked) {
            redirect(303, '/');
        }
    }
}