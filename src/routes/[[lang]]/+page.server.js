
export async function load ({ locals, cookies }) {
    // const profile = await locals.getProfile();
    const session = await locals.safeGetSession();
    // const cookies_ctm_landing_view_state = cookies.get('_ctm_landing_view_state') !== undefined ? JSON.parse(cookies.get('_ctm_landing_view_state')) : false;

    let landingInit = cookies.get('wtm_landing_init');
    if (landingInit == undefined) {
        cookies.set('wtm_landing_init', JSON.stringify(true), {
            path: '/',
            maxAge: 60 * 60 * 24 * 365,
            httpOnly: false,
        });
    }

    const fetchMaps = async () => {
        let query = locals.supabase
            .from('maps')
            .select('*, maps_views(*)')
            .order('created_at', { ascending: false })
        


        const { data, error } = await query
        

		for(const map of data || []) {
			const { data: imageData, error: imageError } = await locals.supabase
				.storage
				.from('maps')
				.getPublicUrl(`${map.url}`)			
			map.preview = imageData.publicUrl;
		}

		return data || [];
    }

    return {
        mapsList: await fetchMaps(),
        landingInit
        // showMapsListState: cookies_ctm_landing_view_state === "true" ? true : false
    }
}


export const actions = {
    changeLandingViewState : async ({ request, cookies }) => {
        const formData = Object.fromEntries(await request.formData());
        // console.log(JSON.parse(formData.showMapsList))
        cookies.set('_ctm_landing_view_state', JSON.stringify(formData.showMapsList), {
                path: '/',
                maxAge: 60 * 60 * 24 * 365,
                httpOnly: false,
            },
        );
    }
}