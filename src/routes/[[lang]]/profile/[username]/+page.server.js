import { z } from 'zod';
import { superValidate } from 'sveltekit-superforms/server';
import { error, fail } from '@sveltejs/kit';

export async function load ({ locals, params }) {
    const { username } = params;

    const fetchUserByUsername = async () => {
        const { data, error } = await locals.supabase
            .from('profiles')
            .select('*, boards ( *, maps (url) )')
            .eq('username', username)
            .single()

        return data;
    }

    const userData = await fetchUserByUsername();

    async function fetchContributedMarkers () {
        if (!userData?.id) return 0;

        let { data, error } = await locals.supabase
            .from('markers')
            .select('id')
            .eq('category', 'contribute')
            .eq('author', userData.id)

        console.log(data, error)

        if (error) return 0;
        return data.length;
    }

    return {
        user: userData,
        contributedMarkers: await fetchContributedMarkers()
    }
}

export const actions = {
    editProfile: async ({ request, locals }) => {
        const session = await locals.getSession();
        if (!session) return;

        const formData = await request.formData();
        const form = await superValidate(formData, editProfileSchema);
        console.log(form)
        if (!form.valid) {
            return fail(400, {
                form
            })
        }

        const { data: userData, error: userError } = await locals.supabase
            .from('profiles')
            .update(form.data)
            .eq('id', session.user.id)
            .select('*')
            .single()

        if (userError) {
            return {
                status: 'error',
                form,
                message: 'Something goes wrong!'
            }
        }

        return {
            form,
            userData
        }
    }    
}

const editProfileSchema = z.object({
    description: z.string().max(200).optional().or(z.literal('')),

    youtube: z.string().optional().or(z.literal('')),
    twitch: z.string().optional().or(z.literal('')),
    kick: z.string().optional().or(z.literal('')),
    tiktok: z.string().optional().or(z.literal('')),
    instagram: z.string().optional().or(z.literal('')),
    website: z.string().optional().or(z.literal('')),
});