
import { PUBLIC_API_ENDPOINT } from '$env/static/public';


export async function GET({ url }) {
    
    const data = await fetch(PUBLIC_API_ENDPOINT + '/api/v1/maps');
    const maps = await data.json();

    const headers = {
        'Cache-Control': 'max-age=0, s-maxage=3600',
        'Content-Type': 'application/xml'
    }

    const render = (maps) => `<?xml version="1.0" encoding="UTF-8" ?>
    <urlset
        xmlns="https://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xhtml="https://www.w3.org/1999/xhtml"
        xmlns:mobile="https://www.google.com/schemas/sitemap-mobile/1.0"
    >
    <url>
        <loc>${url.origin}</loc>
        <changefreq>monthly</changefreq>
    </url>


        ${maps.map((item) => {
            // console.log(item)
            return `
                <url>
                    <loc>${url.origin}/maps/${item.url}</loc>
                    <changefreq>weekly</changefreq>
                </url>
            `
        }).join('')}
    </urlset>`
    const body = render(maps);

	return new Response(body, {
        headers
    });
}