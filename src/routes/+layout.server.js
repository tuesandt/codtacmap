// To make the session available across the UI, including pages and layouts, 
// it is crucial to pass the session as a parameter in the root layout's server load function.
// https://supabase.com/docs/guides/auth/auth-helpers/sveltekit#client-side
export const load = async ({ url, locals: { safeGetSession, getProfile, getAccessRole } }) => {
	console.log('..............................................// main layout.server.js')

	const { session, user } = await safeGetSession()

    return {
		pathname: url.pathname || '/',
		session,
		user,
		profile: await getProfile(),
		accessRole: await getAccessRole()
		// manageRole: await manageRole(),
	};
}
