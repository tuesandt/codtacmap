import { PUBLIC_SUPABASE_ANON_KEY, PUBLIC_SUPABASE_URL } from '$env/static/public';
import { combineChunks, createBrowserClient, isBrowser, parse } from '@supabase/ssr';

export const load = async ({ fetch, data, depends }) =>  {
	depends('supabase:auth')
  
	const supabase = createBrowserClient(PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY, {
	  global: {
		fetch,
	  },
	  cookies: {
		get(key) {
		  if (!isBrowser()) {
			return JSON.stringify(data.session)
		  }
  
		  const cookie = parse(document.cookie)
		  return cookie[key]
		},
	  },
	})
  
	/**
	 * It's fine to use `getSession` here, because on the client, `getSession` is
	 * safe, and on the server, it reads `session` from the `LayoutData`, which
	 * safely checked the session using `safeGetSession`.
	 */
	const {
	  data: { session },
	} = await supabase.auth.getSession()

	const profile = data.profile;
	const accessRole = data.accessRole;
	const user = data.user;

  
	return { supabase, session, profile, user, accessRole, pathname: data.pathname }
  }


// {
// 	console.log('..............................................// main layout.js');
// 	depends('supabase:auth');

// 	const supabase = createBrowserClient(PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY, {
// 		global: {
// 			fetch
// 		},
// 		cookies: {
// 			get(key) {
// 				if (!isBrowser()) {
// 					return JSON.stringify(data.session);
// 				}

// 				const cookie = combineChunks(key, (name) => {
// 					const cookies = parse(document.cookie);
// 					return cookies[name];
// 				});
// 				return cookie;
// 			}
// 		}
// 	});

//     const { data: { session } } = await supabase.auth.getSession()

// 	const profile = data.profile;
// 	const accessRole = data.accessRole;

// 	return { supabase, session, profile, accessRole, pathname: data.pathname };
// }
