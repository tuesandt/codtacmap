import { object } from "zod"

function createMapMarkersStore (initial) {
    console.log('init', initial)
    let list = $state([])

    // function toggle (name) {
    //     list = list.map(el => {
    //         if (el.code_name === name) {
    //             return {
    //                 ...el,
    //                 visible: !el.visible
    //             }
    //         }
    //         return el;
    //     });
    // }

    // function enable (name) {
    //     list = list.map(el => {
    //         if (el.code_name === name) {
    //             return {
    //                 ...el,
    //                 visible: true
    //             }
    //         }
    //         return el;
    //     });
    // }

    return {
        get list() { return list },
        set list(data) { list = data},
        add(marker) {
            if (!list[marker.group]) list[marker.group][list] = [];
            list[marker.group].list = list[marker.group].list.filter(el => el.id !== marker.id);
            list[marker.group].list.push(marker);
            // if (!existingMarker) {
            //     list[marker.group].list.push(marker);
            // }
        },
        remove(marker) {
            console.log('Remove from Store')
            let oldList = list[marker.group].list;
            let updatedGroupList = oldList.filter(el => el.id !== marker.id);
            list[marker.group].list = updatedGroupList;
        }
        // toggle,
        // enable
    }
}


export const MapMarkersStore = createMapMarkersStore()