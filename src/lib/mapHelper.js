import { Feature } from 'ol';
import { Circle, LineString, MultiLineString, Point } from 'ol/geom';
import ImageLayer from 'ol/layer/Image';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Static from 'ol/source/ImageStatic.js';
import { Fill, Icon, Stroke, Style, Text } from 'ol/style';
import { Projection } from 'ol/proj';

export function createPoint(item, icons) {
	const marker = new Feature({
		geometry: new Point([item.location.lng, item.location.lat]),
		id: item.id,
		name: item.name,
		type: item.type,
		codeName: item.group
	});
	let icon = icons.find((i) => i.name == item.icon);
	marker.setStyle(
		new Style({
			image: new Icon({
				// Вкажіть шлях до вашого зображення маркера
				// src: '../assets/images/maps/icons/icon-default.svg',
				src: icon.path,
				anchor: [0.5, 0.5],
				width: icon.size[0],
				height: icon.size[1]
			})
		})
	);
	return marker;
}

export function createLabel(item) {
	const marker = new Feature({
		geometry: new Point([item.location.lng, item.location.lat]),
		id: item.id,
		name: item.name,
		type: item.type,
	});
	// let icon = icons.find(i => i.name == item.icon);
	marker.setStyle(
		new Style({
			text: new Text({
				text: item.name.toUpperCase(),
				font: '12px GeistMono',
				fill: new Fill({ color: '#EEE' }),
				// stroke: new Stroke({ color: '#fff', width: 2 }),
				backgroundFill: new Fill({ color: 'rgba(0, 0, 0, 0.85)' }),
				padding: [6, 10, 5, 12], // Відступи навколо тексту,
			})
		})
	);

	return marker;
}

export function createCircle(item) {
	// const _vectorSource = new VectorSource();
	const marker = new Feature({
		geometry: new Circle([+item.location.lng, +item.location.lat], item.markers_extra.styles.radius),
		id: item.id,
		name: item.name,
		type: item.type
	});
	marker.setStyle(
		new Style({
			fill: new Fill({
				color: `${item.markers_extra.styles.backgroundColor}`
			}),
			stroke: new Stroke({
				width: 3,
				color: `${item.markers_extra.styles.borderColor}`,
				lineDash: [8, 6]
			}),

			// renderer(coordinates, state) {
			// 	const [[x, y], [x1, y1]] = coordinates;
			// 	const ctx = state.context;
			// 	const dx = x1 - x;
			// 	const dy = y1 - y;
			// 	const radius = Math.sqrt(dx * dx + dy * dy);

			// 	const innerRadius = 0;
			// 	const outerRadius = radius * 1.4;

			// 	const gradient = ctx.createRadialGradient(x, y, innerRadius, x, y, outerRadius);
			// 	gradient.addColorStop(0, `${item.markers_extra.styles.backgroundColor}`);
			// 	// gradient.addColorStop(0.5, 'rgba(255,0,0,0.2)');
			// 	// gradient.addColorStop(1, 'rgba(255,0,0,0.8)');
			// 	ctx.beginPath();
			// 	ctx.arc(x, y, radius, 0, 2 * Math.PI, true);
			// 	ctx.fillStyle = gradient;
			// 	ctx.fill();

			// 	ctx.arc(x, y, radius, 0, 2 * Math.PI, true);
			// 	// ctx.strokeStyle = 'rgba(255,0,0,1)';
			// 	ctx.strokeStyle = `${item.markers_extra.styles.borderColor}`;
			// 	ctx.stroke();
			// }
		})
	);

	return marker;
}


export function createLayer (item, icons) {
	const extent = [0, 0, 8000, 8000];
	const projection = new Projection({
		code: 'xkcd-image',
		units: 'pixels',
		extent: extent
	});
	let icon = icons.find((i) => i.name == item.icon);

	const layer = new Static({
		url: icon.path,
		projection: projection,
		imageExtent: extent,
		crossOrigin: 'anonymous',
		// id: item.id,
		// name: item.name,
		// type: item.type,
	})
	return layer;
}

export function createLine(item) {
	const lineFeature = new Feature({
		geometry: new LineString(item.location),
		id: item.id,
		name: item.name,
		type: item.type
	});

	lineFeature.setStyle(
		new Style({
			stroke: new Stroke({
				color: item.markers_extra.styles.borderColor,
				width: item.markers_extra.styles.width,
			})
		})
	)
	// map.addLayer(vectorLayer);

	return lineFeature;
}


export function addGrid(map, {
    extent = [0, 0, 8000, 8000],
	cells = {x: 15, y: 10},
    cellLabel = true,
	sideLabel = true,
	label = { x: 'text', y: 'number' },
	corner = 'top-left',
    opacity = 0.3,
	steps = [],

} = {}) {
    // const extents = [0, 0, 8000, 8000];
    // const opacity = 0.3;

	let gridLayers = {};
		
	gridLayers.base = createGridLayer(extent, {x: cells.x, y: cells.y}, opacity)
	map.addLayer(gridLayers.base);

	if (steps.length) {
		steps.forEach((step) => {
			gridLayers[step.zoom] = createGridLayer(extent, {x: step.cells.x, y: step.cells.y}, opacity, cellLabel = false)
			map.addLayer(gridLayers[step.zoom]);
		})
	}

    // const gridLayers = {
    //     'low': createGridLayer(extent, {x: 10, y: 10}, opacity),
    //     'medium': createGridLayer(extent, {x: 50, y: 50}, opacity),
    //     'high': createGridLayer(extent, {x: 100, y: 100}, opacity)
    // };

    // Initially set visibility based on the current zoom level
    updateLayerVisibility(map.getView().getZoom(), gridLayers);

    // Listen for zoom level changes
    map.getView().on('change:resolution', () => {
        updateLayerVisibility(map.getView().getZoom(), gridLayers);
    });

    // Add layers to the map
    
    // map.addLayer(gridLayers.medium);
    // map.addLayer(gridLayers.high);

    function createGridLayer(extent, cells, opacity) {
        const cellWidth = (extent[2] - extent[0]) / cells.x;
        const cellHeight = (extent[3] - extent[1]) / cells.y;
        const horizontalLines = [];
        const verticalLines = [];

        // Generate horizontal lines
        for (let i = 0; i <= cells.y; i++) {
            let y = extent[1] + i * cellHeight;
            horizontalLines.push([extent[0], y, extent[2], y]);
        }

        // Generate vertical lines
        for (let i = 0; i <= cells.x; i++) {
            let x = extent[0] + i * cellWidth;
            verticalLines.push([x, extent[1], x, extent[3]]);
        }

        const allLines = horizontalLines.concat(verticalLines).map(line => [ [line[0], line[1]], [line[2], line[3]] ]);

        const gridFeature = new Feature(new MultiLineString(allLines));

        const vectorSource = new VectorSource({
            features: [gridFeature]
        });

        const vectorLayer = new VectorLayer({
            source: vectorSource,
            style: new Style({
                stroke: new Stroke({
                    color: `rgba(255, 255, 255, ${opacity})`,
                    width: 1
                })
            }),
			name: 'grid',
            visible: false
        });

        _addGridLabels(vectorSource, extent, cellWidth, cellHeight, cells, cellLabel, sideLabel, label, corner);

        return vectorLayer;
    }

    function updateLayerVisibility(zoom, layers) {
        layers.base.setVisible(layers.base.getVisible());
		steps.forEach((step) => {
			layers[step.zoom].setVisible(step.zoom <= zoom && layers.base.getVisible());
		})
    }

	return gridLayers;
}



export function addGridLines(map, extent, numCells) {
	const cellWidth = (extent[2] - extent[0]) / numCells[0];
	const cellHeight = (extent[3] - extent[1]) / numCells[1];
	const lines = [];

	// Горизонтальні лінії
	for (let i = 0; i <= numCells[1]; i++) {
		let y = extent[1] + i * cellHeight;
		lines.push(
			new Feature(
				new LineString([
					[extent[0], y],
					[extent[2], y]
				])
			)
		);
	}

	// Вертикальні лінії
	for (let i = 0; i <= numCells[0]; i++) {
		let x = extent[0] + i * cellWidth;
		lines.push(
			new Feature(
				new LineString([
					[x, extent[1]],
					[x, extent[3]]
				])
			)
		);
	}

	const vectorSource = new VectorSource({
		features: lines
	});

	const vectorLayer = new VectorLayer({
		source: vectorSource,
		style: new Style({
			stroke: new Stroke({
				color: `rgba(255, 255, 255, ${opacity})`,
				width: 1
			})
		}),
		name: 'grid',
		visible: true
	});

	_addGridLabels(vectorSource, extent, cellWidth, cellHeight, numCells); // Додавання міток

	// map.addLayer(vectorLayer);
	return vectorLayer;
}


export function convertToSector(x, y, mapSize = [8000, 8000], sectorSize = {x: 10, y: 10}) {
	// Map dimensions
	const mapWidth = mapSize[0];
	const mapHeight = mapSize[1];

	// Sector dimensions: 10 sectors horizontally and vertically
	const sectorWidth = mapWidth / sectorSize.x;
	const sectorHeight = mapHeight / sectorSize.y;

	// Calculate sector indices
	let horizontalIndex = Math.floor(x / sectorWidth);
	let verticalIndex = Math.floor((mapHeight - y) / sectorHeight);

	// Ensure the indices are within the bounds
	horizontalIndex = Math.max(0, Math.min(horizontalIndex, 9));
	verticalIndex = Math.max(0, Math.min(verticalIndex, 9));

	// Convert indices to sector labels
	const horizontalLabel = String.fromCharCode('A'.charCodeAt(0) + horizontalIndex);
	const verticalLabel = verticalIndex.toString();

	// Return the combined sector name
	return horizontalLabel + verticalLabel;
}

function _addGridLabels(vectorSource, extent, cellWidth, cellHeight, cells, cellLabel = true, sidelabel = true, label = { 
	type: { x: 'text', y: 'number' },
	position: { x: 'top', y: 'left' }
 }, corner) {

    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const font16 = '16px GeistMono'; // Визначення фонту один раз для використання у стилях
    const font12 = '12px GeistMono'; // Менший фонт для міток комірок
    const grayColor = new Fill({ color: '#555' }); // Основний колір наповнення тексту
    const lightGrayColor = new Fill({ color: '#A0A0A0' }); // Світлий колір для міток комірок

    // Функція для створення стилю з заданим текстом та параметрами
    function createTextStyle(text, offsetX = 0, offsetY = 0, fontSize = font16, fillColor = grayColor) {
        return new Style({
            text: new Text({
                text: text,
                font: fontSize,
                fill: fillColor,
                offsetX: offsetX,
                offsetY: offsetY
            })
        });
    }


	if (sidelabel) {
		// Додавання горизонтальних міток (літери)
		for (let i = 0; i < cells.x; i++) {
			let x = extent[0] + i * cellWidth + cellWidth / 2;
			let horizontalLabelFeature = new Feature({
				geometry: new Point([x, extent[3]]),
				name: alphabet[i] // Літера мітки
			});
			if (label.type.x === 'text') {
				horizontalLabelFeature.setStyle(createTextStyle(alphabet[i], 0, -10));
			} else {
				horizontalLabelFeature.setStyle(createTextStyle(`${i + 1}`, 0, -10));
			}

			vectorSource.addFeature(horizontalLabelFeature);
		}	


		// Додавання вертикальних міток (цифри)
		if (corner === 'bottom-left') {
			for (let i = cells.y; i > 0; i--) {
				let y = extent[0] + i * cellHeight - cellHeight / 2;
				let verticalLabelFeature = new Feature({
					geometry: new Point([extent[0], y]),
					name: `${i}` // Цифра мітки, починаючи з 1
				});
	
				verticalLabelFeature.setStyle(createTextStyle(`${i}`, -20, 0));
				vectorSource.addFeature(verticalLabelFeature);
			}
		} else {
			for (let i = 0; i < cells.y; i++) {
				let y = extent[3] - i * cellHeight - cellHeight / 2;
				let verticalLabelFeature = new Feature({
					geometry: new Point([extent[0], y]),
					name: `${i + 1}` // Цифра мітки, починаючи з 1
				});

				verticalLabelFeature.setStyle(createTextStyle(`${i + 1}`, -20, 0));
				vectorSource.addFeature(verticalLabelFeature);
			}
		}
	}

    // Додавання міток для кожної комірки
    if (cellLabel) {
        for (let i = 0; i < cells.x; i++) {
            for (let j = 0; j < cells.y; j++) {
                let x = extent[0] + i * cellWidth;
                let y = extent[3] - j * cellHeight;

                let cellLabelFeature = new Feature({
                    geometry: new Point([x, y]),
                    name: `${alphabet[i]}${j + 1}` // Нумерація з 1
                });

                
				if (label.type.x === 'text') {
					cellLabelFeature.setStyle(createTextStyle(`${alphabet[i]}${j + 1}`, 10, 10, font12, lightGrayColor));
				} else {
					cellLabelFeature.setStyle(createTextStyle(`${i + 1}${j + 1}`, 10, 10, font12, lightGrayColor));
				}
                vectorSource.addFeature(cellLabelFeature);
            }
        }
    }
}


