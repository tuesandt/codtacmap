function createSidebarNavStore (initial) {
    console.log('init', initial)
    let list = $state([])

    function toggle (name) {
        list = list.map(el => {
            if (el.code_name === name) {
                return {
                    ...el,
                    visible: !el.visible
                }
            }
            return el;
        });
    }

    function enable (name) {
        list = list.map(el => {
            if (el.code_name === name) {
                return {
                    ...el,
                    visible: true
                }
            }
            return el;
        });
    }

    return {
        get list() { return list },
        set list(data) { list = data},
        toggle,
        enable
    }
}


export const SidebarNavStore = createSidebarNavStore()