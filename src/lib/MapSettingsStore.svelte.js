function createMapSettingsStore (initial) {
    let data = $state([])
    
    return {
        get data() { return data },
        set data(newData) { data = newData },
        toggle: (name) => {
            let changed = !data[name]
            data[name] = changed
        }
    }
}


export const MapSettingsStore = createMapSettingsStore()