function createMapCoordinatesStore() {
    let data = $state({
        coords: {lat: 0, lng: 0},
        sector: ''
    })

    return {
        get data() { return data },
        set data(newData) { data = newData },
    }
}

export const MapCoordinates = createMapCoordinatesStore();