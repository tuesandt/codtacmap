function createLastAddedMarkerStore (initial) {
    let data = $state({})

    return {
        get data() { return data },
        set data(newData) { data = newData },
        add: (marker) => {
            data = marker
        }
    }
}

export const lastAddedMarkerStore = createLastAddedMarkerStore()