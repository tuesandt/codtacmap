// indexedDbUtil.js

const DB_NAME = 'WTM.DB';
const DB_VERSION = 1;
const STORE_NAME = 'markersImages';

// Open (or create) our database
function openDb() {
	return new Promise((resolve, reject) => {
		const request = indexedDB.open(DB_NAME, DB_VERSION);

		request.onerror = (event) => {
			console.error('IndexedDB error:', event.target.errorCode);
			reject(event.target.errorCode);
		};

		request.onupgradeneeded = (event) => {
			const db = event.target.result;
			if (!db.objectStoreNames.contains(STORE_NAME)) {
				const objectStore = db.createObjectStore(STORE_NAME, { keyPath: 'id' });
			}
		};

		request.onsuccess = (event) => {
			resolve(event.target.result);
		};
	});
}

// Save an image to the database
async function saveImage(id, name, blob) {
	const db = await openDb();
	const transaction = db.transaction([STORE_NAME], 'readwrite');
	const store = transaction.objectStore(STORE_NAME);
	store.put({ id, name, blob });
	console.log('Saved in indexedDB');
}

// Retrieve an image from the database
async function getImage(id, newName) {
    const db = await openDb();
    return new Promise((resolve, reject) => {
        const transaction = db.transaction([STORE_NAME], 'readwrite'); // Changed to 'readwrite' to allow deletion
        const store = transaction.objectStore(STORE_NAME);
        const request = store.get(id);

        request.onerror = (event) => {
            reject(event.target.errorCode);
        };

        request.onsuccess = async (event) => {
            const data = event.target.result;
            if (data) {
                if (data.name !== newName) {
                    // Name is different, remove the image from the database
                    await store.delete(id);
                    console.log(`Image with id ${id} removed from the database due to name difference.`);
                    resolve(null); // Resolve with null to indicate the image was removed
                } else {
                    // Name matches, resolve with the existing Blob
                    resolve(data.blob);
                }
            } else {
                // No data found for the ID
                resolve(null);
            }
        };
    });
}

export { saveImage, getImage };
