import { writable } from "svelte/store";

export let mapMarkers = writable(null);
export let mapVectorLayers = writable([]);
export let mapCoordinates = writable({ lat: 0, lng: 0 });


// form actions
export let popupFormAction = writable(false);

// settings
export let storeMapSettings = writable({});

export let storeSettingsOpacityMarkers = writable(1);
export let storeSettingsGrayMap = writable(0);
export let storeSettingsMapNoise = writable(1);


//test 
export let storeSidebarNavList = writable([]);