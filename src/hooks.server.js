import { PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY } from '$env/static/public';
import { createServerClient } from '@supabase/ssr';
import { redirect } from '@sveltejs/kit';

export const handle = async ({ event, resolve }) => {
	event.locals.supabase = createServerClient(PUBLIC_SUPABASE_URL, PUBLIC_SUPABASE_ANON_KEY, {
		cookies: {
			get: (key) => event.cookies.get(key),
			/**
			 * Note: You have to add the `path` variable to the
			 * set and remove method due to sveltekit's cookie API
			 * requiring this to be set, setting the path to `/`
			 * will replicate previous/standard behaviour (https://kit.svelte.dev/docs/types#public-types-cookies)
			 */
			set: (key, value, options) => {
				event.cookies.set(key, value, { ...options, path: '/' });
			},
			remove: (key, options) => {
				event.cookies.delete(key, { ...options, path: '/' });
			}
		}
	});

	/**
	 * Unlike `supabase.auth.getSession`, which is unsafe on the server because it
	 * doesn't validate the JWT, this function validates the JWT by first calling
	 * `getUser` and aborts early if the JWT signature is invalid.
	 */
	event.locals.safeGetSession = async () => {
		const {
			data: { user },
			error
		} = await event.locals.supabase.auth.getUser();

		if (error) {
			return { session: null, user: null };
		}
		
		const {
			data: { session }
		} = await event.locals.supabase.auth.getSession();

		return { session, user };
	};


	event.locals.getProfile = async () => {
		let { session } = await event.locals.safeGetSession();

		if (!session) return null;

		const { data: userData } = await event.locals.supabase
			.from('profiles')
			.select('*')
			.eq('id', session.user.id)
			.single();

		return userData;
	};

	event.locals.getAccessRole = () => {
		let roles = ['admin', 'moderator'];
		return roles;
	};

	// let session = await event.locals.safeGetSession();
	// if (session) {
	// 	let { username, is_blocked } = await event.locals.getProfile();
	// 	if (!username) {
	// 		if (!event.url.pathname.startsWith('/username')) {
	// 			redirect(303, '/username');
	// 		}
	// 	}

	// 	if (is_blocked) {
	// 		if (!event.url.pathname.startsWith('/blocked')) {
	// 			redirect(303, '/blocked');
	// 		}
	// 	}
	// }

	return resolve(event, {
		/**
		 * There´s an issue with `filterSerializedResponseHeaders` not working when using `sequence`
		 *
		 * https://github.com/sveltejs/kit/issues/8061
		 */
		filterSerializedResponseHeaders(name) {
			return name === 'content-range';
		},

		// transformPageChunk: ({ html }) => html.replace('%lang%', get_lang(event))
		transformPageChunk: ({ html }) => html.replace('%lang%', event.params.lang || 'en')
	});
};
